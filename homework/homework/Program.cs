﻿using homework.models;
using System;

namespace homework
{
    class Program
    {
        static void Main(string[] args)
        {
            using(ApplicationDbContext db = new ApplicationDbContext())
            {
                Organization organization = new Organization()
                {
                    Address = "Chuy",
                    Name = "Dantist Service",
                    Supervisor = "Dontist"
                };

                db.Add(organization);
                db.SaveChanges();

                Patient patient = new Patient()
                {
                    Name = "Mike",
                    Surname = "Hawkins",
                    Sex = 'm',
                    DateOfBirth = new DateTime(1995, 01, 10),
                    DateOfAccCreate = DateTime.Now,
                    OrganizationID = organization.ID,
                };

                db.Add(patient);
                db.SaveChanges();

                var user = new User()
                {
                    FullName = "Maksimillian",
                    Login = "Maks",
                    OrganizationID = organization.ID,
                    Passord = "Passord"
                };

                db.Add(user);
                db.SaveChanges();

                Visit visit = new Visit()
                {
                    PatrientID = patient.ID,
                    UserID = user.ID,
                    VisitStatus = VisitStatus.a,
                    VisitType = VisitType.a,
                    DateOfVisit = DateTime.Now.AddDays(10),
                    Price = 2000m
                };

                db.Add(visit);
                db.SaveChanges();
            }
        }
    }
}
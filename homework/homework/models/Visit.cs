﻿using homework.interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace homework.models
{
    public class Visit : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("Patient")]
        public int PatrientID { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }
        public VisitType VisitType { get; set; }
        public DateTime DateOfVisit { get; set; }
        public VisitStatus VisitStatus { get; set; }
        public decimal? Price { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual User User { get; set; }
    }
}
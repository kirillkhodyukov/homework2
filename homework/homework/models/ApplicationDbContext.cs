﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace homework.models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Organization> Organizations { get; set; } 
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<DentalChart> DentalCharts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Teeth> Teeths { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Users\User\Desktop\с# 4 модуль\homework2\homework\homework");
            builder.AddJsonFile("appsetting.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}

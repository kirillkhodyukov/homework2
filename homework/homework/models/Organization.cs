﻿using homework.interfaces;

namespace homework.models
{
    public class Organization : IEntity<int>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Supervisor { get; set; }
    }
}

﻿using homework.interfaces;

namespace homework.models
{
    public class Teeth : IEntity<int>
    {
        public int ID { get; set; }
        public JawType JawType { get; set; }
        public SideOfTheJaw SideOfTheJaw { get; set; }
        public int ToothNumber { get; set; }
    }
}

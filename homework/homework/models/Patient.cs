﻿using homework.interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace homework.models
{
    public class Patient : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public DateTime DateOfBirth { get; set; }
        public char Sex { get; set; }
        public DateTime DateOfAccCreate { get; set; }
        public virtual Organization Organization { get; set; }
    }
}

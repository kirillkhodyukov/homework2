﻿using homework.interfaces;
using System.ComponentModel.DataAnnotations.Schema;

namespace homework.models
{
    public class DentalChart : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("Teeth")]
        public int TeethID { get; set; }

        [ForeignKey("Visit")]
        public int VisitID { get; set; }
        public string Info_Complaints { get; set; }
        public string Comment { get; set; }
        public Teeth Teeth { get; set; }
        public Visit Visit { get; set; }
    }
}

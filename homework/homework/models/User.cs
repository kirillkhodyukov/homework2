﻿using homework.interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace homework.models
{
    public class User : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationID { get; set; }

        [Required]
        public string Login { get; set; }
        public string FullName { get; set; }

        [Required]
        [MinLength(4)]
        public string Passord { get; set; }

        [Required]
        public bool Blocked { get; set; }

        public DateTime DateOfCreate { get 
            {
                return DateTime.Now;
            } set 
            {
            
            } 
        }
        public virtual Organization Organization { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework.interfaces
{
    public interface IEntity<T>
    {
        public T ID { get; set; }
    }
}